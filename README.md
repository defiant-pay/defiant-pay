# DeFiant Pay #



### What is DeFiant Pay? ###

**DeFiant Pay** is a browser extension that facilitates access to **Web Monetization**, **Rafiki**, **Interledger**, and **MetaMask Snaps** for receiving and making payments.